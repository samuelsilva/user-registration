# Gamesys User Exclusion List
## user-registration

### Run project

`./start.sh`

or

**Mac** `./gradlew bootRun`

**Windows** `gradlew bootRun`

## cURL

### Valid user
```
curl --location --request POST 'http://localhost:8080/user-registration?=' \
--header 'Content-Type: application/json' \
--data-raw '{
	"username":"username",
	"password":"Pass01",
	"dateOfBirth":"2010-06-20",
	"ssn":"123456789"
}'
```

### User on exclusion list
```
curl --location --request POST 'http://localhost:8080/user-registration?=' \
--header 'Content-Type: application/json' \
--data-raw '{
	"username":"konradZuse",
	"password":"zeD1",
	"dateOfBirth":"1910-06-22",
	"ssn":"987654321"
}'
```

### Run tests

**Mac** `./gradlew test`

**Windows** `gradlew test`

### Open H2 [Database](http://localhost:8080/h2-console)

**Use JDBC URL**: `jdbc:h2:mem:testdb`

### Open [Swagger](http://localhost:8080/swagger-ui.html)

## Improvements

- Adding controller paths in a separate file
- Adding logs
- Adding messages to a property file
- Adding uniqueness validation to username

## Assumptions

- Username do not needs validation
- It's not necessary an endpoint so save users on exclusion list

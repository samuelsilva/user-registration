package com.gamesys.userregistration.controller.exception;

/**
 * Custom exception handler to invalid username error
 */
public class UsernameValidationException extends RuntimeException {

    public UsernameValidationException(String message) {
        super(message);
    }
}

package com.gamesys.userregistration.controller.exception;

/**
 * Custom exception handler to invalid password error
 * @author samuelsilva
 */
public class PasswordValidationException extends RuntimeException {

    public PasswordValidationException(String message) {
        super(message);
    }
}

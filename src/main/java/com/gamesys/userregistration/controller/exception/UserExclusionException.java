package com.gamesys.userregistration.controller.exception;

/**
 * Custom exception handler to user in exclusion list error
 */
public class UserExclusionException extends RuntimeException {

    public UserExclusionException(String message) {
        super(message);
    }
}

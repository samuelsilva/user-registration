package com.gamesys.userregistration.controller.exception;

/**
 * Custom exception handler to existing user error
 * @author samuelsilva
 */
public class ExistingUserValidationException extends RuntimeException {

    public ExistingUserValidationException(String message) {
        super(message);
    }
}

package com.gamesys.userregistration.controller;

import com.gamesys.userregistration.controller.exception.*;
import com.gamesys.userregistration.model.User;
import com.gamesys.userregistration.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * User registration controller
 * @author samuesilva
 */
@Controller
@RequestMapping("/user-registration")
public class UserRegistrationController {

    private UserService userService;
    private RestResponseEntityExceptionHandler exceptionHandler;

    @Autowired
    public UserRegistrationController(UserService userService, RestResponseEntityExceptionHandler exceptionHandler) {
        this.userService = userService;
        this.exceptionHandler = exceptionHandler;
    }

    /**
     * Save valid user
     * @param user
     * @return Saved user
     * @throws UsernameValidationException
     * @throws PasswordValidationException
     * @throws UserExclusionException
     * @throws ExistingUserValidationException
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity save(@RequestBody User user) throws UsernameValidationException, PasswordValidationException,
            UserExclusionException, ExistingUserValidationException {
        return new ResponseEntity(userService.save(user), HttpStatus.CREATED);
    }
}

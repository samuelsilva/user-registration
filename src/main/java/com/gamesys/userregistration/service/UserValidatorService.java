package com.gamesys.userregistration.service;

import com.gamesys.userregistration.controller.exception.PasswordValidationException;
import com.gamesys.userregistration.controller.exception.UsernameValidationException;

/**
 * Service to offer validation of a user.
 * @author samuelsilva
 */
public interface UserValidatorService {

    /**
     * Validate username
     * @param username
     * @return true if username is valid
     */
    boolean validateUsername(String username) throws UsernameValidationException;

    /**
     * Validate password
     * @param password
     * @return true if password is valid
     */
    boolean validatePassword(String password) throws PasswordValidationException;
}

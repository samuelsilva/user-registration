package com.gamesys.userregistration.service;

import com.gamesys.userregistration.controller.exception.PasswordValidationException;
import com.gamesys.userregistration.controller.exception.UsernameValidationException;
import org.springframework.stereotype.Service;
import java.util.regex.Pattern;

@Service
public class UserValidatorServiceImp implements UserValidatorService {

    private static final String PASSWORD_PATTERN = "(^.*(?=.{4,})(?=.*\\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$)";
    private static final Pattern PATTERN = Pattern.compile(PASSWORD_PATTERN);

    @Override
    public boolean validateUsername(String username) throws UsernameValidationException {
        if(username == null || username.isEmpty() || username.contains(" ")) {
            throw new UsernameValidationException("Username must not contain spaces");
        }

        return true;
    }

    @Override
    public boolean validatePassword(String password) throws PasswordValidationException {
        if (password != null && PATTERN.matcher(password).matches()) {
            return true;
        }

        throw new PasswordValidationException("Password needs at least four characters, " +
                "at least one lower case character, at least one upper case character, at least one number");
    }
}

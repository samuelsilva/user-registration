package com.gamesys.userregistration.service;

import com.gamesys.userregistration.controller.exception.ExistingUserValidationException;
import com.gamesys.userregistration.controller.exception.PasswordValidationException;
import com.gamesys.userregistration.controller.exception.UserExclusionException;
import com.gamesys.userregistration.controller.exception.UsernameValidationException;
import com.gamesys.userregistration.model.User;
import com.gamesys.userregistration.repository.UserRepository;
import com.gamesys.userregistration.repository.mapper.UserEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UserServiceImp implements UserService {

    private ExclusionService exclusionService;
    private UserRepository repository;
    private UserValidatorService validator;

    @Autowired
    public UserServiceImp(ExclusionService exclusionService, UserRepository repository, UserValidatorService validator) {
        this.exclusionService = exclusionService;
        this.repository = repository;
        this.validator = validator;
    }

    @Override
    public User save(User user) throws UsernameValidationException, PasswordValidationException,
            UserExclusionException, ExistingUserValidationException {

        if(exclusionService.validate(user.getDateOfBirth().toString(), user.getSsn().toString())) {
            if (validator.validateUsername(user.getUsername()) && validator.validatePassword(user.getPassword()) &&
                    validateExistingUser(user.getDateOfBirth(), user.getSsn())) {

                UserEntityMapper mapper = new UserEntityMapper();
                return mapper.from(repository.save(mapper.to(user)));
            }
        }

        throw new UserExclusionException("User on exclusion list");
    }

    private boolean validateExistingUser(LocalDate dateOfBirth, Long ssn) throws ExistingUserValidationException {
        if(!repository.findByDateOfBirthAndSsn(dateOfBirth, ssn).isPresent()) {
            return true;
        }

        throw new ExistingUserValidationException("User already exists");
    }
}

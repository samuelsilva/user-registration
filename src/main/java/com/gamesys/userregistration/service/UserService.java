package com.gamesys.userregistration.service;

import com.gamesys.userregistration.controller.exception.ExistingUserValidationException;
import com.gamesys.userregistration.controller.exception.PasswordValidationException;
import com.gamesys.userregistration.controller.exception.UserExclusionException;
import com.gamesys.userregistration.controller.exception.UsernameValidationException;
import com.gamesys.userregistration.model.User;

/**
 * User CRUD service
 */
public interface UserService {

    /**
     * Save valid user
     * @param user
     * @return Saved user
     * @throws UsernameValidationException
     * @throws PasswordValidationException
     * @throws UserExclusionException
     * @throws ExistingUserValidationException
     */
    User save(User user) throws UsernameValidationException, PasswordValidationException,
            UserExclusionException, ExistingUserValidationException;
}

package com.gamesys.userregistration.service;

import com.gamesys.userregistration.repository.UserExclusionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExclusionServiceImp implements ExclusionService {

    private UserExclusionRepository repository;

    @Autowired
    public ExclusionServiceImp(UserExclusionRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean validate(String dateOfBirth, String ssn) {
        return !repository.findByDateOfBirthAndSsn(dateOfBirth, ssn).isPresent();
    }
}

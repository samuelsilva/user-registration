package com.gamesys.userregistration.repository;

import com.gamesys.userregistration.repository.entity.UserExclusionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository to manage user exclusion list data persistence
 * @author samuelsilva
 */
@Repository
public interface UserExclusionRepository extends JpaRepository<UserExclusionEntity, Long> {

    Optional<UserExclusionEntity> findByDateOfBirthAndSsn(String dateOfBirth, String ssn);
}

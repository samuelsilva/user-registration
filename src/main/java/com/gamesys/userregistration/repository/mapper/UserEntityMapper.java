package com.gamesys.userregistration.repository.mapper;

import com.gamesys.userregistration.model.User;
import com.gamesys.userregistration.repository.entity.UserEntity;

/**
 * User Entity mapper to/from User entity
 * @author samuelsilva
 */
public class UserEntityMapper {

    public User from(UserEntity user) {
        return new User(user.getUsername(), user.getPassword(), user.getDateOfBirth(), user.getSsn());
    }

    public UserEntity to(User user) {
        return new UserEntity(null, user.getUsername(), user.getPassword(), user.getDateOfBirth(), user.getSsn());
    }
}

package com.gamesys.userregistration.repository;

import com.gamesys.userregistration.repository.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Repository to manage user data persistence
 * @author samuelsilva
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByDateOfBirthAndSsn(LocalDate dateOfBirth, Long ssn);
}

package com.gamesys.userregistration.repository.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Getter
@Setter
@Entity
public class UserExclusionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private String dateOfBirth;

    private String ssn;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserExclusionEntity userExclusionEntity = (UserExclusionEntity) o;
        return dateOfBirth.equals(userExclusionEntity.dateOfBirth) &&
                ssn.equals(userExclusionEntity.ssn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateOfBirth, ssn);
    }
}

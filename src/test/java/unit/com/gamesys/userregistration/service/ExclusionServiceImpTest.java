package unit.com.gamesys.userregistration.service;

import com.gamesys.userregistration.repository.UserExclusionRepository;
import com.gamesys.userregistration.repository.entity.UserExclusionEntity;
import com.gamesys.userregistration.service.ExclusionServiceImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ExclusionServiceImpTest {

    @InjectMocks
    private ExclusionServiceImp service;

    @Mock
    private UserExclusionRepository repository;

    @Test
    public void shouldRetrieveTrueIfUserIsNotExcluded() {
        Mockito.when(repository.findByDateOfBirthAndSsn("1993-12-06", "113624786"))
                .thenReturn(Optional.ofNullable(null));

        assertTrue(service.validate("1993-12-06", "113624786"));
    }

    @Test
    public void shouldRetrieveFalseIfUserIsExcluded() {
        Mockito.when(repository.findByDateOfBirthAndSsn("1993-12-06", "113624786"))
                .thenReturn(Optional.ofNullable(new UserExclusionEntity()));

        assertFalse(service.validate("1993-12-06", "113624786"));
    }

}
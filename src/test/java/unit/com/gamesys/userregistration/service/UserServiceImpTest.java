package unit.com.gamesys.userregistration.service;

import com.gamesys.userregistration.model.User;
import com.gamesys.userregistration.repository.UserRepository;
import com.gamesys.userregistration.repository.entity.UserEntity;
import com.gamesys.userregistration.repository.mapper.UserEntityMapper;
import com.gamesys.userregistration.service.ExclusionService;
import com.gamesys.userregistration.service.UserServiceImp;
import com.gamesys.userregistration.service.UserValidatorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImpTest {

    @InjectMocks
    private UserServiceImp service;

    @Mock
    private UserRepository repository;

    @Mock
    private ExclusionService exclusionService;

    @Mock
    private UserValidatorService validatorService;

    @Spy
    private UserEntityMapper mapper;

    private static final LocalDate DATE = LocalDate.now();
    private static final User USER = getMockedUser();
    private static final UserEntity USER_ENTITY = getMockedUserEntity();

    @Test
    public void shouldSaveValidUser() {
        when(exclusionService.validate(DATE.toString(), "1")).thenReturn(true);
        when(validatorService.validatePassword(USER.getPassword())).thenReturn(true);
        when(validatorService.validateUsername(USER.getUsername())).thenReturn(true);
        when(repository.findByDateOfBirthAndSsn(DATE, 1L)).thenReturn(Optional.empty());
        when(repository.save(USER_ENTITY)).thenReturn(USER_ENTITY);
        User savedUser = service.save(USER);

        assertEquals(savedUser, USER);
        assertEquals(savedUser.getUsername(), USER.getUsername());
        assertEquals(savedUser.getPassword(), USER.getPassword());
    }

    private static User getMockedUser() {
        return new User("username", "Pass1", DATE, 1L);
    }

    private static UserEntity getMockedUserEntity() {
        return new UserEntity(1L,"username", "Pass1", DATE, 1L);
    }
}

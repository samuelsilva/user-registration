package unit.com.gamesys.userregistration.service;


import com.gamesys.userregistration.controller.exception.PasswordValidationException;
import com.gamesys.userregistration.controller.exception.UsernameValidationException;
import com.gamesys.userregistration.service.UserValidatorServiceImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class UserValidatorServiceImpTest {

    @InjectMocks
    private UserValidatorServiceImp service;

    @Test
    public void shouldRetrieveTrueIfValidUsername() {
        assertTrue(service.validateUsername("Test"));
    }

    @Test(expected = UsernameValidationException.class)
    public void shouldRetrieveExceptionIfInvalidUsername() {
        service.validateUsername("Te st");
    }

    @Test
    public void shouldRetrieveTrueIfValidPassword() {
        assertTrue(service.validatePassword("Test1"));
    }

    @Test(expected = PasswordValidationException.class)
    public void shouldRetrieveExceptionIfInvalidPassword() {
        service.validatePassword("test");
    }
}
package unit.com.gamesys.userregistration.repository.mapper;

import com.gamesys.userregistration.model.User;
import com.gamesys.userregistration.repository.entity.UserEntity;
import com.gamesys.userregistration.repository.mapper.UserEntityMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import java.time.LocalDate;
import static org.junit.Assert.assertEquals;

@SpringBootTest@RunWith(MockitoJUnitRunner.class)
public class UserEntityMapperTest {

    private static final LocalDate DATE = LocalDate.now();

    @Spy
    private UserEntityMapper mapper;

    @Test
    public void shouldRetrieveUser() {
        User mappedUser = mapper.from(getMockedUserEntity());
        User user = getMockedUser();
        assertEquals(mappedUser, user);
        assertEquals(mappedUser.getUsername(), user.getUsername());
        assertEquals(mappedUser.getPassword(), user.getPassword());
    }

    @Test
    public void shouldRetrieveUserEntity() {
        Assert.assertEquals(mapper.to(getMockedUser()), getMockedUserEntity());

        UserEntity mappedUserEntity = mapper.to(getMockedUser());
        UserEntity userEntity = getMockedUserEntity();
        assertEquals(mappedUserEntity, userEntity);
        assertEquals(mappedUserEntity.getUsername(), userEntity.getUsername());
        assertEquals(mappedUserEntity.getPassword(), userEntity.getPassword());
    }

    private User getMockedUser() {
        return new User("username", "Pass1", DATE, 1L);
    }

    private UserEntity getMockedUserEntity() {
        return new UserEntity(1L,"username", "Pass1", DATE, 1L);
    }
}

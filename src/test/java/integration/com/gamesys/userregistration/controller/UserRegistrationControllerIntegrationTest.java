package integration.com.gamesys.userregistration.controller;

import com.gamesys.userregistration.UserRegistrationApplication;
import com.gamesys.userregistration.controller.UserRegistrationController;
import com.gamesys.userregistration.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserRegistrationApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserRegistrationControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private UserRegistrationController controller;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    private static final LocalDate DATE = LocalDate.now();

    @Test
    public void shouldSaveValidUser() {
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/user-registration", getMockedUser(
                        "username", "Test1", DATE, 1L), String.class);

        assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    public void shouldRetrieve400InvalidUsername() {
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/user-registration", getMockedUser(
                        "user name", "Test1", DATE, 1L), String.class);

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals("Username must not contain spaces", responseEntity.getBody());
    }

    @Test
    public void shouldRetrieve400InvalidPassword() {
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/user-registration", getMockedUser(
                        "username", "Test", DATE, 1L), String.class);

        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals("Password needs at least four characters, at least one lower case character," +
                " at least one upper case character, at least one number", responseEntity.getBody());
    }

    @Test
    public void shouldRetrieve403UserOnExclusionList() {
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/user-registration", getMockedUser(
                        "konradZuse","zeD1",
                        LocalDate.of(1910, 06, 22), 987654321L), String.class);

        assertEquals(403, responseEntity.getStatusCodeValue());
        assertEquals("User on exclusion list", responseEntity.getBody());
    }

    @Test
    public void shouldRetrieve409UserAlreadyExists() {
        restTemplate.postForEntity(
                "http://localhost:" + port + "/user-registration", getMockedUser(
                        "username", "Test1", DATE, 1L), String.class);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/user-registration", getMockedUser(
                        "username", "Test1", DATE, 1L), String.class);

        assertEquals(409, responseEntity.getStatusCodeValue());
        assertEquals("User already exists", responseEntity.getBody());
    }

    private static User getMockedUser(String username, String pass, LocalDate date, Long ssn) {
        return new User(username, pass, date, ssn);
    }
}

package integration.com.gamesys.userregistration;

import com.gamesys.userregistration.UserRegistrationApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UserRegistrationApplication.class})
public class UserRegistrationApplicationTests {

	@Test
	public void contextLoads() {
	}

}
